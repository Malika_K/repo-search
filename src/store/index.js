import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const route = 'https://api.github.com/search/repositories';

function initialState() {
  return {
    query: '',
    data: [],
    loading: false,
  };
}

const set = (key) => (state, val) => {
  state[key] = val;
};

export default new Vuex.Store({
  state: initialState,

  getters: {
    data: (state) => state.data,
    loading: (state) => state.loading,
  },

  mutations: {
    setQuery: set('query'),
    setData: set('data'),
    setLoading: set('loading'),
    resetState(state) {
      Object.assign(state, initialState());
    },
  },

  actions: {
    fetchData({ commit, state }) {
      commit('setLoading', true);
      const q =
        '?q=' + encodeURIComponent(state.query + ' in:name org:microsoft');
      axios
        .get(route + q)
        .then((response) => {
          commit(
            'setData',
            response.data.items.sort((a, b) => (a.name > b.name ? 1 : -1))
          );
        })
        .catch((error) => {
          console.log('Get error while fetching data', error);
        })
        .finally(() => {
          commit('setLoading', false);
        });
    },
    setQuery({ commit }, value) {
      commit('setQuery', value);
    },
    resetState({ commit }) {
      commit('resetState');
    },
  },
});
