# GitHub Repository Search

The task is to create a simple tool for searching company's repositories on GitHub by name.

- Design

  - Font: https://fonts.google.com/specimen/Roboto

- API

  - Documentation: https://docs.github.com/en/free-pro-team@latest/rest/reference/repos#list-organization-repositories
  - Example: https://api.github.com/orgs/microsoft/repos

- Implementation details
  - Search results should appear whenever more than 2 characters were entered and there are matches.
  - Spinner should appear while data is being fetched.
  - Search results should be sorted alphabetically in an ascending order (A to Z).
  - Search results should disappear when clicked outside.
  - Search query should be removed when clicked on ✕.
  - Browsers: latest WebKit-based browsers, latest Mozilla Firefox.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```
